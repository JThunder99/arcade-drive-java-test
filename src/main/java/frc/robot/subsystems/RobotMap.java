package frc.robot.subsystems;

public class RobotMap {

	//TalonSRX's
	public static final int DRIVETRAIN_LEFT_FRONT_TALONSRX = 1;
	public static final int DRIVETRAIN_LEFT_BACK_TALONSRX = 2;
	public static final int DRIVETRAIN_RIGHT_FRONT_TALONSRX = 4;
	public static final int DRIVETRAIN_RIGHT_BACK_TALONSRX = 3;


	//Joystick
	public static final int RobotContainer_DRIVER_CONTROLLER = 0;
	public static final int DRIVER_CONTROLLER_MOVE_AXIS = 1;
	public static final int DRIVER_CONTROLLER_ROTATE_AXIS = 4;

}
