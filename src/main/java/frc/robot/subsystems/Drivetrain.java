/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.commands.DriveArcade;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

public class Drivetrain extends SubsystemBase {
  /**
   * Creates a new Drivetrain.
   */
  WPI_TalonSRX leftFrontTalonSRX = null;
  WPI_TalonSRX leftBackTalonSRX = null;
  WPI_TalonSRX rightFrontTalonSRX = null;
  WPI_TalonSRX rightBackTalonSRX = null;

  DifferentialDrive differentialDrive = null;
  
  public Drivetrain() {
    leftFrontTalonSRX = new WPI_TalonSRX(RobotMap.DRIVETRAIN_LEFT_FRONT_TALONSRX);
    leftBackTalonSRX = new WPI_TalonSRX(RobotMap.DRIVETRAIN_LEFT_BACK_TALONSRX);
    rightFrontTalonSRX = new WPI_TalonSRX(RobotMap.DRIVETRAIN_RIGHT_FRONT_TALONSRX);
    rightBackTalonSRX = new WPI_TalonSRX(RobotMap.DRIVETRAIN_RIGHT_BACK_TALONSRX);

    final SpeedControllerGroup leftMotors = new SpeedControllerGroup(leftFrontTalonSRX, leftBackTalonSRX);
    final SpeedControllerGroup rightMotors = new SpeedControllerGroup(rightFrontTalonSRX, rightBackTalonSRX);

    differentialDrive = new DifferentialDrive(leftMotors, rightMotors);
  }


  public void arcadeDrive(double moveSpeed, double rotateSpeed){
    differentialDrive.arcadeDrive(moveSpeed, rotateSpeed);
  }


  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
  protected void initDefaultCommand() {
    setDefaultCommand(new DriveArcade());
  }

}
